/*  
  Thanks to Aquatic Elf for help with understanding C# dictionary and 
  hash tables.
  
  Simon Omega
  
  Usage:  Spawn a treasure chest that produces loot based on character luck 
          (maxed at 100) and profession (skill level). Loot is directly given 
          to the player. Only one player can activate a treasure chest at a 
          time.
          This should not be game breaking. The with maximum luck and all
          perfect RNG, the best item will only have 5 attributes and even then
          attribute value will be between 10 and 100. If the player has perfect
          RNG and all 5 get 100%... They are lucky enough to deserve it.
          
          m_debug = false; Turn this to true to see output in the console about
            every looting mobile and objects generated. Handy when changing or 
            adding items to the loot.
            THIS DISABLES CONSOLE COMANDS FOR SOME REASON, YOU WILL NEED TO USE
            IN GAME COMMANDS.
            
          m_minSkill = 25; Set this to the minimum skill required for skill 
            specific items.
            
          KeepRecords = true; Will cause the treasure chest to save the looting
            history during shutdown and reboots.
            This makes looting records persistent. If set to False no loot 
            tables are saved, and all players can loot the box again after 
            reboot.
          Set using m_record via code. KeepRecords from in game.
          
          RelootMinutes = ( 60 * 24 ); How many minutes before a player can
            loot this same box again. Default is 24 hours.
          Set using m_minutesTillLoot via code. RelootMinutes from in game.
          
          Examples:
            KeepRecords = true; m_minutesTillLoot = ( 60 * 24 );
              Players can loot every 24 hours, this counter is persistent 
              across reboots and players must wait 24 hours.
            KeepRecords = true; m_minutesTillLoot = ( 60 * 24 * 7 * 52 );
              Players can loot every year, this counter is persistent 
              across reboots and players must wait 24 hours
            KeepRecords = false; m_minutesTillLoot = ( 60 * 24 * 7 );
              Players can loot every week, or after a server reboot.
            KeepRecords = false; m_minutesTillLoot = ( 60 * 24 * 7 * 52 * 100 );
              Players only loot after a server reboot. the timer is set to 100 
              years. If your server runs for 100 years without a reboot...
              I will Slow Clap. O_O

  Updated - 2020 Jan 12
*/

using System;
using System.Collections;
using System.Collections.Generic;
using Server.Items;
using Server.Mobiles;

namespace Server.Items
{
  [FlipableAttribute( 0x9A9, 0xE7E, 0xE3D, 0xE3C, 0x9AA, 0xE7D, 0x9A8, 0xE80, 0xE77, 0xE43, 0xE42 )]
  public class OneShotTreasureContainer : BaseContainer
  {
    /* 
      Fillable Containers have many quirks that make this object a pain if we base it off fillable containers.
      It was far easier to incorporate the code from fill containers that we needed and base this off stanrard containers.
      It also saves memory.
      Borrowing Constructors from Server.Loot. FYI: Server.Loot is a class not a name-space.
    */
    public static Item Construct(Type type)
    {
        Item item;
        try
        {
            item = Activator.CreateInstance(type) as Item;
        }
        catch
        {
            return null;
        }
    
        return item;
    }
    
    public static Item Construct(Type[] types)
    {
        if (types.Length > 0)
        {
            return Construct(types, Utility.Random(types.Length));
        }
    
        return null;
    }
    
    public static Item Construct(Type[] types, int index)
    {
        if (index >= 0 && index < types.Length)
        {
            return Construct(types[index]);
        }
    
        return null;
    }
    
    public static Item Construct(params Type[][] types)
    {
        int totalLength = 0;
    
        for (int i = 0; i < types.Length; ++i)
        {
            totalLength += types[i].Length;
        }
    
        if (totalLength > 0)
        {
            int index = Utility.Random(totalLength);
    
            for (int i = 0; i < types.Length; ++i)
            {
                if (index >= 0 && index < types[i].Length)
                {
                    return Construct(types[i][index]);
                }
    
                index -= types[i].Length;
            }
        }
    
        return null;
    }
    /*
      End Borrow
    */
    
    private bool m_record = false;
    // Do we keep looting rules persistent across reboots?
    [CommandProperty( AccessLevel.GameMaster )]
    public bool KeepRecords 
    { 
      get
      {
        return m_record;
      } 
      set
      {
        if ( value )
        {
          m_record = true;
        }
        else
        {
          m_record = false;
        }
      }
    }
    
    private double m_minutesTillLoot = ( 60 * 24 );
    // How long until a player can loot this chest again [in minutes]? (Default 24 Hours)
    [CommandProperty( AccessLevel.GameMaster )]
    public double RelootMinutes
    { 
      get
      {
        return m_minutesTillLoot;
      } 
      set
      {
        m_minutesTillLoot = (double)value;
      }
    }
    
    protected bool IsLooting = false;
    // Is this chest currently being looted by another player?

    private Dictionary<Mobile, DateTime> m_lootTable = new Dictionary<Mobile, DateTime>();
    // Loot Tracker Dictionary

    // No longer used, retained for refrence.
    /* 
    public bool CanLootTreasure ( Mobile actor )
    {
      PlayerMobile IsPlayer = actor as PlayerMobile;

      // actor is not a Player type
      if ( IsPlayer == null )
        return false;

      // actor is a Player type
      // actor has no loot record
      if ( !m_lootTable.ContainsKey( IsPlayer ) )
        return true;

      // actor has a loot record
      // and the loot record has expired
      if ( DateTime.Now > m_lootTable[IsPlayer] )
        return true;

      // actor has a loot record
      // and the loot record has NOT expired
      // Or we have a combination that should not exist so default action is to deny/false.
      return false;
    }*/
    
    private bool m_debug = false;
    // Output debug lines.
    
     private int m_minSkill = 25;
    // Minimum Skill requiered to generate skill based loot.
    
    [Constructable]
    public OneShotTreasureContainer() : 
    base( Utility.RandomList( 0x9A9, 0xE7E, 0xE3D, 0xE3C, 0x9AA, 0xE7D, 0x9A8, 0xE80, 0xE77, 0xE43, 0xE42 ) )
    {
      Movable = false;
      // Fixed location so players can not pick up the chests.
      Weight = 150.0;
    }
    
    public override void AddNameProperty( ObjectPropertyList list )
    // Add Name to Property List
    {
      string name = this.Name;
      if ( name == null )
        list.Add( LabelNumber );
      else
        list.Add( name );
    }
    
    public override void GetProperties( ObjectPropertyList list )
    // Display Property List
    {
      AddNameProperty( list );
      list.Add( "Holding equipment for necessitous warriors" );
    }


    public override void Open( Mobile from )
    // Over Ride the Open Method
    {
      if ( IsLooting || this.Deleted )
      // If the chest is open already stop all open requests until it is closed.
      {
        from.SendLocalizedMessage( 500291, "", 0x22 );
        //  500291    Someone else is using that.
        //  1075215   This object has already been activated.
        return;
      }

      IsLooting = true;
      // The chest is being looted. See above check.
      
      PlayerMobile IsPlayer = from as PlayerMobile;
      if ( IsPlayer != null )
      {
      
        if (m_debug) 
        { 
          Console.WriteLine( $"DEBUG: OneShot - {IsPlayer.Name:G} attempted loot at {DateTime.Now:G}" );
        }
        
        if ( !m_lootTable.ContainsKey( IsPlayer ) )
        // First time looting
        {
          DateTime whenLoot = DateTime.Now.AddMinutes( m_minutesTillLoot );
          m_lootTable[IsPlayer] = whenLoot;
          //Give up the Goods
          generateLoot( IsPlayer );
          
          if ( m_debug ) 
          { 
            Console.WriteLine( $"DEBUG: OneShot - {IsPlayer.Name:G} completed loot." );
          }
        
        }       
        else
        // Player looted once before
        {
        
          if (m_debug) 
          {
            Console.WriteLine( $"DEBUG: OneShot - {IsPlayer.Name:G} previously looted." );
          }
            
          if ( DateTime.Now > m_lootTable[IsPlayer] )
          // Time to loot again.
          {
          
            generateLoot( IsPlayer );
            DateTime whenLoot = DateTime.Now.AddMinutes( m_minutesTillLoot );
            m_lootTable[IsPlayer] = whenLoot;
            
            if ( m_debug ) 
            { 
              Console.WriteLine( $"DEBUG: OneShot - {IsPlayer.Name:G} completed loot." );
            }
            
          }
          else
          // Not time to loot.
          {
          
            if ( m_debug ) 
            { 
              Console.WriteLine( $"DEBUG: OneShot - {IsPlayer.Name:G} can loot again: {m_lootTable[IsPlayer]:G}" );
            }
            
            IsPlayer.SendLocalizedMessage( 1080420, "", 0x22 );  
            //  1080420  You\92ve already found the treasure here.
            
          }
        }
      } // End Player Check
      
      IsLooting = false;
      // The chest is being looted. See above check.
      if (m_debug) 
      { 
        Console.WriteLine( $"DEBUG: OneShot - {IsPlayer.Name:G} Open() opertaion compelte." );
      }
      
    }

    // Disable droping items into / on the container.
    public override bool TryDropItem(Mobile from, Item dropped, bool sendFullMessage)
    {
      return false;
    }
    public override bool OnDragDropInto(Mobile from, Item item, Point3D p)
    {
      return false;
    }


    #region Item Type Lists
    /*
      Item type lists for loot generation. Add items types here if you wish.
    */
    
    public static readonly Type[] OneShotSwordTypes = new[]
    {
      typeof(Axe), typeof(DoubleAxe), typeof(Broadsword), typeof(Katana), typeof(Kryss), typeof(Longsword)
    };

    public static readonly Type[] OneShotMaceTypes = new[]
    {
      typeof(Mace), typeof(Maul), typeof(BlackStaff), typeof(QuarterStaff), typeof(Pickaxe)
    };
    
    public static readonly Type[] OneShotFencingTypes = new[]
    {
      typeof(ShortSpear), typeof(Kryss), typeof(Dagger)
    };
    
    public static readonly Type[] OneShotBowTypes = new[] 
    {
      typeof(Bow), typeof(Crossbow)
    };
    
    public static readonly Type[] OneShotClothingTypes = new[]
    {
      typeof(Cloak), typeof(WizardsHat), typeof(BodySash), typeof(Doublet), typeof(Boots), typeof(Sandals), typeof(Shirt), typeof(FancyShirt), typeof(LongPants), typeof(Robe), typeof(HalfApron)
    };
    
    public static readonly Type[] OneShotArmorTypes = new[]
    {
      typeof(LeatherArms), typeof(LeatherChest), typeof(LeatherGloves), typeof(LeatherGorget), typeof(LeatherLegs), typeof(PlateGorget), typeof(RingmailArms), typeof(RingmailChest), typeof(RingmailGloves), typeof(RingmailLegs)
    };
        
    /*
      End item type lists.
    */
    #endregion
    
    #region Chance Generation - Simplified for new players
    public double playerChance( Mobile target ) 
    // A decimal (percentage) value between 1 and Player.Luck (capped at 100).
    // This is intended to help new players, veterans can get some small stuff
    // with their 2,000 luck.
    {
      return ( Math.Max( 1, Math.Min( 100, target.Luck ) ) / 100 );
    } 

    public int chanceToInt( double chance ) 
    // Convert double into a value 1 to (double * 10).
    // using playerChance as your double will result in a value 1 to 5 generated
    // from the playerChance (Luck capped at 100).
    {
      return Convert.ToInt32( Math.Max( 1, Convert.ToInt32( ( chance * 10 ) / 2 ) ) );
    }
    #endregion
    
    /*
      All the magic.
    */
    void generateLoot( Mobile actor ) 
    {
      PlayerMobile IsPlayer = actor as PlayerMobile;
      if ( IsPlayer == null )
        return;

       Item item;
      // Buffer variable for Items added to Player.
      
      double chance = 0.0;
      // Buffer for chance calculations. This way we can generate multiple 
      // chance values unique to each item.
      
      int attributeCount = 0;
      // Buffer for attribute counts. 

      IsPlayer.SendLocalizedMessage( 503047, "", 0x22 ); 
      //  503047    You take some treasure and put it in your backpack.
      
      // Random Treasure Map
      //item = new TreasureMap( Utility.RandomMinMax( 0, 1 ), Map.Trammel );
      item = new TreasureMap( Utility.RandomMinMax( 0, 1 ), IsPlayer.Map );
      if ( !IsPlayer.AddToBackpack( item ) )
      {
        IsPlayer.SendLocalizedMessage( 1060157, "", 0x22 ); 
        //  1060157    Your backpack is too full, and it falls to the 
        //    ground.
        //  1077182  Your backpack is too full, and it falls to the ground.
        //  1062783  You cannot take that because your pack is too full.
        //  1060157  Your backpack is too full to carry all the loot.
        item.MoveToWorld( IsPlayer.Location, IsPlayer.Map );
      }
      if ( m_debug ) 
      { 
        Console.WriteLine( $"DEBUG: OneShot - {IsPlayer.Name:G} Generated item: {item:G}" );
      }
      // End Treasure Map
      
      // Random Gold
      item = new Gold( Utility.RandomMinMax( 10, Math.Max( 25, IsPlayer.Luck ) ) );
      // Give any where between 10 and Player.Luck gold.
      if ( !IsPlayer.AddToBackpack( item ) )
      {
        IsPlayer.SendLocalizedMessage( 1060157, "", 0x22 ); 
        //  1060157    Your backpack is too full, and it falls to the 
        //    ground.
        item.MoveToWorld( IsPlayer.Location, IsPlayer.Map );
      }
      if ( m_debug ) 
      { 
        Console.WriteLine( $"DEBUG: OneShot - {IsPlayer.Name:G} Generated item: {item:G}" );
      }
      // End Gold
      
      // Random Armor (runic)
      item = Construct( OneShotArmorTypes ) as BaseArmor;
      if ( Core.AOS )
      {
        chance = playerChance( IsPlayer );
        attributeCount = chanceToInt( chance );
        BaseRunicTool.ApplyAttributesTo( item, attributeCount, Convert.ToInt32( chance * 10 ), Convert.ToInt32( chance * 100 ) );
        // Random Runic enhancements applied attribute with power between 1/10th and 100% of the random chance.
      }
      item.Hue = Utility.RandomMetalHue();
      if ( !IsPlayer.AddToBackpack( item ) )
      {
        IsPlayer.SendLocalizedMessage( 1060157, "", 0x22 ); 
        //  1060157    Your backpack is too full, and it falls to the ground.
        item.MoveToWorld( IsPlayer.Location, IsPlayer.Map );
      }
      if ( m_debug ) 
      { 
        Console.WriteLine( $"DEBUG: OneShot - {IsPlayer.Name:G} Generated item: {item:G}" );
      }
      // End Armor
     
      // Random Cloth (runic)
      item = Construct( OneShotClothingTypes ) as BaseClothing;
      if ( Core.AOS )
      {
        chance = playerChance( IsPlayer );
        attributeCount = chanceToInt( chance );
        BaseRunicTool.ApplyAttributesTo( item, attributeCount, Convert.ToInt32( chance * 10 ), Convert.ToInt32( chance * 100 ) );
        // Random Runic enhancements applied attribute with power between 1/10th and 100% of the random chance.
      }
      item.Hue = Utility.RandomNeutralHue();
      if ( !IsPlayer.AddToBackpack( item ) )
      {
        IsPlayer.SendLocalizedMessage( 1060157, "", 0x22 ); 
        //  1060157    Your backpack is too full, and it falls to the ground.
        item.MoveToWorld( IsPlayer.Location, IsPlayer.Map );
      }
      // End Cloth

      // Random Weak Runic Hammer
      if ( (int)IsPlayer.Skills[SkillName.Blacksmith].Value >= 30 )
      { 
        //item = new RunicHammer(CraftResource.Iron + Utility.RandomMinMax( 0, 4 ), Core.AOS ? (25) : 20 );
        item = new RunicHammer( CraftResource.Iron + Utility.RandomMinMax( 0, 4 ), 20 );
        if ( !IsPlayer.AddToBackpack( item ) )
        {
          IsPlayer.SendLocalizedMessage( 1060157, "", 0x22 ); 
          //  1060157    Your backpack is too full, and it falls to the ground.
          item.MoveToWorld( IsPlayer.Location, IsPlayer.Map );
        }
        if ( m_debug ) 
        { 
          Console.WriteLine( $"DEBUG: OneShot - {IsPlayer.Name:G} Generated item: {item:G}" );
        }
      }
      // End Runic Hammer

      // Random Ingots
      chance = playerChance( IsPlayer );
      attributeCount = chanceToInt( chance );
      if ( (int)IsPlayer.Skills[SkillName.Blacksmith].Value >= m_minSkill )
      { 
        switch ( Utility.RandomMinMax( 0, 2 ) )
	      {
		      case 0:
		      {
			      item = new IronIngot( (int)attributeCount * 4 );
			      break;
		      }
		      case 1:
		      {
			      item = new DullCopperIngot( (int)attributeCount * 4 );
			      break;
		      }
		      case 2:
		      {
			      item = new ShadowIronIngot( (int)attributeCount * 4 );
			      break;
		      }
		      default:
		      {
		        item = new IronIngot( (int)attributeCount * 4 );
		        break;
		      }
        };
        if ( !IsPlayer.AddToBackpack( item ) )
        {
          IsPlayer.SendLocalizedMessage( 1060157, "", 0x22 ); 
          //  1060157    Your backpack is too full, and it falls to the ground.
          item.MoveToWorld( IsPlayer.Location, IsPlayer.Map );
        }
        if ( m_debug ) 
        { 
          Console.WriteLine( $"DEBUG: OneShot - {IsPlayer.Name:G} Generated item: {item:G}" );
        }
      }          
      // End Ingots

      // Random Weak Runic Sewing Kit
      if ( (int)IsPlayer.Skills[SkillName.Tailoring].Value >= m_minSkill )
      { 
        //item = new RunicHammer(CraftResource.Iron + Utility.RandomMinMax( 0, 4 ), Core.AOS ? (25) : 20 );
        item = new RunicSewingKit( CraftResource.RegularLeather + Utility.RandomMinMax( 0, 2 ), 20 );
        if ( !IsPlayer.AddToBackpack( item ) )
        {
          IsPlayer.SendLocalizedMessage( 1060157, "", 0x22 ); 
          //  1060157    Your backpack is too full, and it falls to the ground.
          item.MoveToWorld( IsPlayer.Location, IsPlayer.Map );
        }
        if ( m_debug ) 
        { 
          Console.WriteLine( $"DEBUG: OneShot - {IsPlayer.Name:G} Generated item: {item:G}" );
        }
      }
      // End Sewing

      // Random Leather
      chance = playerChance( IsPlayer );
      attributeCount = chanceToInt( chance );
      if ( (int)IsPlayer.Skills[SkillName.Tailoring].Value >= m_minSkill )
      { 
        switch ( Utility.RandomMinMax( 0, 2 ) )
	      {
		      case 0:
		      {
			      item = new Leather( (int)attributeCount * 4 );
			      break;
		      }
		      case 1:
		      {
			      item = new SpinedLeather( (int)attributeCount * 4 );
			      break;
		      }
		      case 2:
		      {
			      item = new HornedLeather( (int)attributeCount * 4 );
			      break;
		      }
		      default:
		      {
		        item = new Leather( (int)attributeCount * 4 );
		        break;
		      }
        };
        if ( !IsPlayer.AddToBackpack( item ) )
        {
          IsPlayer.SendLocalizedMessage( 1060157, "", 0x22 ); 
          //  1060157    Your backpack is too full, and it falls to the ground.
          item.MoveToWorld( IsPlayer.Location, IsPlayer.Map );
        }
        if ( m_debug ) 
        { 
          Console.WriteLine( $"DEBUG: OneShot - {IsPlayer.Name:G} Generated item: {item:G}" );
        }
      }          
      // End Leather
      
      // Random Sword
      if ( (int)IsPlayer.Skills[SkillName.Swords].Value >= m_minSkill )
      {
        item = Construct( OneShotSwordTypes ) as BaseWeapon;
        if ( Core.AOS )
        {
          chance = playerChance( IsPlayer );
          attributeCount = chanceToInt( chance );
          BaseRunicTool.ApplyAttributesTo( item, attributeCount, Convert.ToInt32( chance * 10 ), Convert.ToInt32( chance * 100 ) );
          // Random Runic enhancements applied attribute with power between 1/10th and 100% of the random chance.
        }
        item.Hue = Utility.RandomMetalHue();
        if ( !IsPlayer.AddToBackpack( item ) )
        {
          IsPlayer.SendLocalizedMessage( 1060157, "", 0x22 ); 
          //  1060157    Your backpack is too full, and it falls to the ground.
          item.MoveToWorld( IsPlayer.Location, IsPlayer.Map );
        }
        if ( m_debug ) 
        { 
          Console.WriteLine( $"DEBUG: OneShot - {IsPlayer.Name:G} Generated item: {item:G}" );
        }
      }
      // End Sword
      
      // Random Mace
      if ( (int)IsPlayer.Skills[SkillName.Macing].Value >= m_minSkill )
      {
        item = Construct( OneShotMaceTypes ) as BaseWeapon;
        if ( Core.AOS )
        {
          chance = playerChance( IsPlayer );
          attributeCount = chanceToInt( chance );
          BaseRunicTool.ApplyAttributesTo( item, attributeCount, Convert.ToInt32( chance * 10 ), Convert.ToInt32( chance * 100 ) );
          // Random Runic enhancements applied attribute with power between 1/10th and 100% of the random chance.
        }
        item.Hue = Utility.RandomMetalHue();
        if ( !IsPlayer.AddToBackpack( item ) )
        {
          IsPlayer.SendLocalizedMessage( 1060157, "", 0x22 ); 
          //  1060157    Your backpack is too full, and it falls to the ground.
          item.MoveToWorld( IsPlayer.Location, IsPlayer.Map );
        }
        if ( m_debug ) 
        { 
          Console.WriteLine( $"DEBUG: OneShot - {IsPlayer.Name:G} Generated item: {item:G}" );
        }
      }
      // End Mace
      
      // Random Fencing
      if ( (int)IsPlayer.Skills[SkillName.Fencing].Value >= m_minSkill )
      {
        item = Construct( OneShotFencingTypes ) as BaseWeapon;
        if ( Core.AOS )
        {
          chance = playerChance( IsPlayer );
          attributeCount = chanceToInt( chance );
          BaseRunicTool.ApplyAttributesTo( item, attributeCount, Convert.ToInt32( chance * 10 ), Convert.ToInt32( chance * 100 ) );
          // Random Runic enhancements applied attribute with power between 1/10th and 100% of the random chance.
        }
        item.Hue = Utility.RandomMetalHue();
        if ( !IsPlayer.AddToBackpack( item ) )
        {
          IsPlayer.SendLocalizedMessage( 1060157, "", 0x22 ); 
          //  1060157    Your backpack is too full, and it falls to the ground.
          item.MoveToWorld( IsPlayer.Location, IsPlayer.Map );
        }
        if ( m_debug ) 
        { 
          Console.WriteLine( $"DEBUG: OneShot - {IsPlayer.Name:G} Generated item: {item:G}" );
        }
      }
      // End Fencing
      
      // Random Bow
      if ( (int)IsPlayer.Skills[SkillName.Archery].Value >= m_minSkill )
      {
        item = Construct( OneShotBowTypes ) as BaseWeapon;
        if ( Core.AOS )
        {
          chance = playerChance( IsPlayer );
          attributeCount = chanceToInt( chance );
          BaseRunicTool.ApplyAttributesTo( item, attributeCount, Convert.ToInt32( chance * 10 ), Convert.ToInt32( chance * 100 ) );
          // Random Runic enhancements applied attribute with power between 1/10th and 100% of the random chance.
        }
        item.Hue = Utility.RandomMetalHue();
        if ( !IsPlayer.AddToBackpack( item ) )
        {
          IsPlayer.SendLocalizedMessage( 1060157, "", 0x22 ); 
          //  1060157    Your backpack is too full, and it falls to the ground.
          item.MoveToWorld( IsPlayer.Location, IsPlayer.Map );
        }
        if ( m_debug ) 
        { 
          Console.WriteLine( $"DEBUG: OneShot - {IsPlayer.Name:G} Generated item: {item:G}" );
        }
        // Add Arrows
        item = new Arrow( (int)attributeCount * 4 );
        if ( !IsPlayer.AddToBackpack( item ) )
        {
          IsPlayer.SendLocalizedMessage( 1060157, "", 0x22 ); 
          //  1060157    Your backpack is too full, and it falls to the ground.
          item.MoveToWorld( IsPlayer.Location, IsPlayer.Map );
        }
        if ( m_debug ) 
        { 
          Console.WriteLine( $"DEBUG: OneShot - {IsPlayer.Name:G} Generated item: {item:G}" );
        }
      }
      // End Bow
      
      // Random Reagent
	    if ( ( (int)IsPlayer.Skills[SkillName.Magery].Value >= m_minSkill ) || ( (int)IsPlayer.Skills[SkillName.Alchemy].Value >= m_minSkill ) || ( (int)IsPlayer.Skills[SkillName.Inscribe].Value >= m_minSkill ) )
      {
        chance = playerChance( IsPlayer );
        attributeCount = chanceToInt( chance );
        for ( int i = attributeCount; i > 0; i-- ) 
        {
	        item = Loot.RandomPossibleReagent( );
	        item.Amount = ( (int)attributeCount * 4 );
	        if ( !IsPlayer.AddToBackpack( item ) )
          {
            IsPlayer.SendLocalizedMessage( 1060157, "", 0x22 ); 
            //  1060157    Your backpack is too full, and it falls to the ground.
            item.MoveToWorld( IsPlayer.Location, IsPlayer.Map );
          }
          if ( m_debug ) 
          { 
            Console.WriteLine( $"DEBUG: OneShot - {IsPlayer.Name:G} Generated item: {item:G}" );
          }
        }
	    }
      // End Reagent
    }

    #region Serialize
    public OneShotTreasureContainer( Serial serial ) : base( serial )
    {
    }

    public override void Serialize( GenericWriter writer )
    {
      base.Serialize( writer );
      writer.Write( (int) 1 );
      writer.Write( (bool) m_record );
      writer.Write( (double) m_minutesTillLoot );
      // Serialize can not write a dictionary. So we write the Key (mobile), 
      // Loot Check (bool), and LastTime (DateTime) in sequence.
      if (m_record)
      {
        writer.Write( (int) m_lootTable.Count );
        foreach( KeyValuePair<Mobile, DateTime> data in m_lootTable )
        {
          writer.Write( (Mobile) data.Key );
          writer.Write( (DateTime) data.Value );
        }
      }
    }

    public override void Deserialize( GenericReader reader )
    {
      base.Deserialize( reader );
      int version = reader.ReadInt();
      bool has_records = reader.ReadBool();
      double has_lootMinutes = reader.ReadDouble();
      // Remember Serialize can not write a dictionary. We read the data 
      //  sequentially and build the dictionary.
      if ( has_records )
      {
        KeepRecords = has_records;
        RelootMinutes = has_lootMinutes;
        int count = reader.ReadInt();
        for( int i = 0; i < count; i++ )
        {
          m_lootTable.Add( reader.ReadMobile(), reader.ReadDateTime() );
        }
      }
      IsLooting = false;
    }
    #endregion
    
  }
}


