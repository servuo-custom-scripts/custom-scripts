using System;
using System.Collections;
using Server.Mobiles;
using Server.Engines.Craft;


namespace Server.Items
{
  [ Alterable(typeof(DefBlacksmithy), typeof(DiscMace)) ]
  [ Flipable( 0xDF1, 0xDF0, 0xE8a, 0xE89 ) ]
  public class StaffOfFireAndIce : BaseStaff
  {

    private static readonly string _StaffOfFireAndIceSwap = "StaffOfFireAndIceSwap";

    private static readonly int _SwapMinutes = 1;
    private static readonly bool _SilenceMessages = false;

    public override bool IsArtifact => true;
    public override int ArtifactRarity => 5;

    public override WeaponAbility PrimaryAbility => WeaponAbility.DoubleStrike;
    public override WeaponAbility SecondaryAbility => WeaponAbility.Disarm;

    public override int StrengthReq => 0;
    public override int IntelligenceReq => 50;

    public override int MinDamage => 10;
    public override int MaxDamage => 14;
    public override float Speed => 2.0f;
    // Incase you use this on a PreAOS server.
    public override int InitMinHits => 31;
    public override int InitMaxHits => 90;

    public override bool CanRepair => true;

    [Constructable]
    public StaffOfFireAndIce() : base ( Utility.RandomList( 0xDF1, 0xDF0, 0xE8a, 0xE89 ) )
    {
      Slayer = SlayerName.Silver;
      Attributes.SpellChanneling = 1;
      Attributes.CastSpeed = -1;
      Resource = CraftResource.ShadowIron;
      MaxRange = 2;
      
      // Weapon Attributes are private members only.
      // They will only be randomized once.
      WeaponAttributes.MageWeapon = 10;
      WeaponAttributes.HitLeechHits = 10;
      WeaponAttributes.HitLeechMana = 20;
      Weight = 5.5;

      //SwapState Attributes
      Attributes.WeaponSpeed = 20;
      Attributes.WeaponDamage = 20;
      WeaponAttributes.HitFireArea = 0;
      WeaponAttributes.ResistFireBonus = 0;
      AosElementDamages[AosElementAttribute.Fire] = 0;
      AosElementDamages[AosElementAttribute.Cold] = 100;
      WeaponAttributes.HitColdArea = 50;
      WeaponAttributes.ResistColdBonus = 20;
      Hue = 0x04AB; // Twisted Blue
      // Hue = 0x077C; // Glossy Blue
      // Hue = 0x054F; // Fire Orange
      
      // Recalculate Durability
      ScaleDurability();

      this.Name = "an <BASEFONT COLOR=#0099FF>Icey</font> Staff";
      
      if ( !TimerRegistry.HasTimer( _StaffOfFireAndIceSwap, this ) )
      {
        TimerRegistry.Register( _StaffOfFireAndIceSwap, this, TimeSpan.FromMinutes( _SwapMinutes ), false, fc => fc.SwapState() );
      }
    }

    public StaffOfFireAndIce(Serial serial) : base(serial)
    {
    }

    public override void AddNameProperty( ObjectPropertyList list )
    {
      // Generic Name Override. Keep Name Classic.
      string name = this.Name;
      if ( name == null )
        list.Add( LabelNumber );
      else
        list.Add( name );
      //list.Add( 1049644, "Intelligence Requirement 40");
      list.Add( $"Intelligence Requirement {IntelligenceReq}" );
    }

    public virtual void SwapState()
    /*
      Set to virtual incase you create a weapon based off this and wish to chnage the SwapState actions.
    */
    {

      PlayerMobile is_player = this.Parent as PlayerMobile;
      if (WeaponAttributes.HitColdArea > 0)
      {
        WeaponAttributes.HitColdArea = 0;
        WeaponAttributes.ResistColdBonus = 0;
        AosElementDamages[AosElementAttribute.Cold] = 0;
        AosElementDamages[AosElementAttribute.Fire] = 100;
        WeaponAttributes.HitFireArea = 50;
        WeaponAttributes.ResistFireBonus = 20;
        Hue = 0x054F; // Fire Orange
        this.Name = "a <BASEFONT COLOR=#FF9900>Firey</font> Staff";
        if ( is_player != null && !_SilenceMessages )
        {
          is_player.SendLocalizedMessage( 1049644, "Your hands are warmed as the staff begins to transmute." );
        }
      }
      else
      {
        WeaponAttributes.HitFireArea = 0;
        WeaponAttributes.ResistFireBonus = 0;
        AosElementDamages[AosElementAttribute.Fire] = 0;
        AosElementDamages[AosElementAttribute.Cold] = 100;
        WeaponAttributes.HitColdArea = 50;
        WeaponAttributes.ResistColdBonus = 20;
        Hue = 0x04AB; // Twisted Blue
        this.Name = "an <BASEFONT COLOR=#0099FF>Icey</font> Staff";
        if ( is_player != null && !_SilenceMessages )
        {
          is_player.SendLocalizedMessage( 1049644, "Your hands become frigid as the staff begins to transmute." );
        }

        // Randomize Damage and Speed on Swap
        MinimumDamage = Utility.RandomMinMax(16, 23);
        MaximumDamage = Utility.RandomMinMax(24, 36);
        WeaponSpeed = (float)System.Math.Round( Utility.RandomMinMax(1.5, 2.5), 2);
      }

      TimerRegistry.UpdateRegistry( _StaffOfFireAndIceSwap, this, TimeSpan.FromMinutes( _SwapMinutes ) );
    }

    public override void Serialize(GenericWriter writer)
    {
      base.Serialize(writer);
      writer.Write(0); // version
    }

    public override void Deserialize(GenericReader reader)
    {
      base.Deserialize(reader);
      int version = reader.ReadInt();
      if ( !TimerRegistry.HasTimer( _StaffOfFireAndIceSwap, this ) )
      {
        TimerRegistry.Register( _StaffOfFireAndIceSwap, this, TimeSpan.FromMinutes( _SwapMinutes ), false, fc => fc.SwapState() );
      }
    }
  }
}
