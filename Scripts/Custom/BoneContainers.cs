/*  
  An attempt to impliment bone contrainers, and add the ability to dye them.
  
  Simon Omega
  
  Thanks to JoshyB, AlphaDragon, Thagoras, and Hammerhand for their 
  Disscussion: 
  http://www.runuo.com/community/threads/help-with-bone-container.490861/

  Updated: 2021 Jan 12
*/

using System;
using Server.Mobiles;
using Server.Multis;

namespace Server.Items
{
  /*
    Base Bone Containers
  */
  [FlipableAttribute( 0x0ECA, 0x0ECB, 0x0ECC, 0x0ECD, 0x0ECE, 0x0ECF, 0x0ED0, 0x0ED1, 0x0ED2 )]
  public class BoneContainer : TrapableContainer, IDyable
  {

    // Use Corpse Gump
    public override int DefaultGumpID { 
      get { 
        return 0x9; 
      } 
    } 

    public override int DefaultMaxWeight
    {
      get
      {
        return 250;
      }
    }

    public bool Dye(Mobile from, DyeTub sender)
    {
      if (Deleted)
        return false;

      Hue = sender.DyedHue;

      return true;
    }

    public BoneContainer( bool can_move, bool do_lift ) : base( 0x0ECA + Utility.Random(9) )
    {
      Movable = can_move;
      LiftOverride = do_lift;
      Weight = 5.0;
    }

    public BoneContainer( bool can_move ) : this( can_move, false )
    {
    }

    [Constructable]
    public BoneContainer() : this( true, false )
    {
    }

    public override void AddNameProperty( ObjectPropertyList list )
    {
      // Generic Name Override. Keep Name Classic.
      string name = this.Name;
      if ( name == null )
        list.Add( LabelNumber );
      else
        list.Add( name );
    }
    
    public override void GetProperties( ObjectPropertyList list )
    {
      // Properties List Diplay Classic Data.
      AddNameProperty( list );
      list.Add( 1050044, "{0}\t{1}", TotalItems, TotalWeight ); // ~1_COUNT~ items, ~2_WEIGHT~ stones
    }

    public BoneContainer( Serial serial ) : base( serial )
    {
    }

    public override void Serialize( GenericWriter writer )
    {
      base.Serialize( writer );
      writer.Write( (int) 1 );
    }

    public override void Deserialize( GenericReader reader )
    {
      base.Deserialize( reader );
      int version = reader.ReadInt();
    }

  }
}
